import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var list = ["Adventure", "Sports", "Tennis", "Baseball",
  "Adventure", "Sports", "Tennis", "Baseball","Adventure",
   "Sports", "Tennis", "Baseball","Adventure", "Sports", "Tennis", 
   "Baseball","Adventure", "Sports", "Tennis", 
   "Baseball","Adventure", "Sports", "Tennis", "Baseball"];

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: new Scaffold(
      appBar: new AppBar(
        title: new Text('Chip Picker List'),
      ),
      body: new Container(
        child: new Column(
          // Or Row or whatever :)
          children: createChildrenTexts(),
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        ),
      ),
    ));
  }

  List<Container> createChildrenTexts() {
    List<Container> childrenTexts = List<Container>();
    for (String name in list) {
      childrenTexts.add(
        Container(
          child: Chip(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            label: Text(
              name,
              style: new TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.deepPurple,
          ),
        ),
      );
    }
    return childrenTexts;
  }
}
